# Contributor: Jonah Brüchert <jbb@kaidan.im>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-angelfish
pkgver=1.2.0
pkgrel=0
pkgdesc="Small Webbrowser for Plasma Mobile"
url="https://phabricator.kde.org/source/plasma-angelfish/"
arch="all"
license="GPL-3.0-or-later"
depends="kirigami2 plasma-framework purpose"
makedepends="cmake qt5-qtwebengine-dev kdeclarative-dev kirigami2-dev plasma-framework-dev kio-dev ki18n-dev extra-cmake-modules purpose-dev"
source="$pkgname-$pkgver.tar.gz::https://invent.kde.org/jbbgameich/plasma-angelfish/-/archive/v$pkgver/plasma-angelfish-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

prepare() {
	default_prepare

	mkdir "$builddir"/build
}

build() {
	cd "$builddir"/build
	cmake "$builddir" \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	make
}

check() {
	cd "$builddir"/build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"/build
	DESTDIR="$pkgdir" make install
}

sha512sums="b0614edb4647200dd3ca255121ad0055f2c63cdd2e8629ddf1823ce852917e453c0d1b6a23de9bfe2ddee388bea9a217df3880cf90e276bb603270ded186fcc6  plasma-angelfish-1.2.0.tar.gz"
